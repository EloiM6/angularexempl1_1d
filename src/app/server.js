var express = require('express');
var app = express();
var mysql = require('mysql');
var cors = require('cors')
var bodyParser = require('body-parser');
app.use(cors())
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));
app.get('/', function (req, res) {
  return res.send({ error: false, message: 'hello' })
});
// set port
app.listen(3000, function () {
  console.log('Node app is running on port 3000');
});
module.exports = app;
// connection configurations
var dbConn = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: 'super3',
  database: 'game'
});
// connect to database
dbConn.connect();
app.get('/test', function (req, res) {
  dbConn.query('SELECT * FROM users', function (error, results, fields) {
    if (error) throw error;
    return res.send({ error: false, data: results, message: 'Superzing list.' });
  });
});
app.get('/get/:correu', function (req, res) {
  let correu = req.params.correu;
  dbConn.query('SELECT * FROM users where nom=?', correu, function (error, results, fields) {
    if (error) throw error;
    return res.send({ error: false, data: results, message: 'Select Ok' });
  });
});
app.post('/get', function (req, res) {
  let correu = req.body.nombre ;
  let alias = req.body.password ;

  dbConn.query('SELECT * FROM users where nom=? and alias =?', [correu,alias], function (error, results, fields) {
    if (error) throw error;
    return res.send({ error: false, data: results, message: 'User list.' });
  });

});
app.post('/insert', function (req, res) {
  let id = req.body.nom;
  let pegy = req.body.pegy;
  let preu = req.body.preu;

  dbConn.query("INSERT INTO videogame set ?", {Nom:id, Pegy: pegy,Preu:preu}, function (error, results, fields) {
    if (error) throw error;

    return res.send({ error: false, data: results, message: 'Insert Ok.' });
  });


});


