export interface Hero {
  id: number;
  name: string;
  emotion?: string;
}
export let heroesdeldesierto: Hero[]= [{id:0,name:"Desastre1", emotion:"tiro en la cara"},
    {id:1, name:"Lado oscuro", emotion:"triste"},
  {id:2, name:"Caballero Oscuro", emotion:"Siempre enfado"}];
/*
export const heroes: Hero[] = [
  { id: 1, name: 'Dr Nice',  emotion: 'happy'},
  { id: 2, name: 'Narco',     emotion: 'sad' },
  { id: 3, name: 'Windstorm', emotion: 'confused' },
  { id: 4, name: 'Magneta'}
];*/
