import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {FormControl, FormGroup} from "@angular/forms";

@Injectable({
  providedIn: 'root'
})
export class ConnectBDService {
  REST_API: string = 'http://localhost:3000'
  constructor(private httpclient:HttpClient) { }
  public conectarBD():Observable<any>{
    return this.httpclient.get(`${this.REST_API}/test`);
  }
  public conectarBD2():Observable<any>{
    return this.httpclient.get(`${this.REST_API}/`);
  }
  public conectarBDGet(nombre : string):Observable<any>{
    console.log(nombre);
    return this.httpclient.get(`${this.REST_API}/get/${ nombre}`);
  }
  public conectarBDPost(form : FormGroup):Observable<any>{
    return this.httpclient.post(`${this.REST_API}/get/`, form);
  }
  public conectarBDPost2(form : FormControl):Observable<any>{
    return this.httpclient.post(`${this.REST_API}/get/`, form);
  }
  public conectarBDInsert(form : FormGroup):Observable<any>{
    return this.httpclient.post(`${this.REST_API}/insert/`, form);
  }

}
