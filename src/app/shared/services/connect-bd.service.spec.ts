import { TestBed } from '@angular/core/testing';

import { ConnectBDService } from './connect-bd.service';
import {HttpClient} from "@angular/common/http";
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {forbiddenNameValidator} from "../../view/ex-form2/validacion.directive";

describe('Servicio de BD', () => {
  let service: ConnectBDService;
  let httpClientSpy: jasmine.SpyObj<HttpClient>;
  let httpClient: HttpClient;

  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get','post']);
    service = new ConnectBDService(httpClientSpy);
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ],providers: [ ConnectBDService ]
    });
    httpClient = TestBed.inject(HttpClient);
    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(ConnectBDService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
  it('Caso 1 de HTTPClient Sin parámetros', () => {
    service.conectarBD().subscribe({
      next :res => {},
      error:fail
    });
    const req = httpTestingController.expectOne(service.REST_API + "/test");
    expect(req.request.method).toEqual('GET');
  });
  it('Caso 2 de HTTPClient Con parámetros y GET', () => {
    let a : FormControl = new FormControl('marc@ies-sabadell.cat')
    service.conectarBDGet(a.value).subscribe({
      next :res => {},
      error:fail
    });
    // Ojo!! Si descomenteu la linia d'abaix i comenteu la següent, podeu comprovar com falla
    //const req = httpTestingController.expectOne(service.REST_API + "/get/marcs@ies-sabadell.cat");
    const req = httpTestingController.expectOne(service.REST_API + "/get/marc@ies-sabadell.cat");

    expect(req.request.method).toEqual('GET');
  });
  it('Caso 3 de HTTPClient Con parámetros y POST', () => {
    let a: FormGroup = new FormGroup({
      nombre : new FormControl('marc@ies-sabadell.cat'),
      password : new FormControl('gat', )
    });

    service.conectarBDPost(a).subscribe({
      next: heroes => {
      },
      error: fail
    });
    const req = httpTestingController.expectOne(service.REST_API + "/get/");
    expect(req.request.method).toEqual('POST');
    expect(req.request.body.value.nombre).toEqual("marc@ies-sabadell.cat")
  });
});
