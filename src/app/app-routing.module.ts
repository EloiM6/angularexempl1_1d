import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from "@angular/router";
import {EjemploRutaComponenteComponent} from "./view/ejemplo-ruta-componente/ejemplo-ruta-componente.component";
import {EjemploRutaComponente2Component} from "./view/ejemplo-ruta-componente2/ejemplo-ruta-componente2.component";
const  routes:  Routes  = [
  {path:'Ruta1', component:EjemploRutaComponenteComponent},
  {path:'Ruta2', component:EjemploRutaComponente2Component}
]


@NgModule({
  declarations: [],
  exports: [RouterModule],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ]
})
export class AppRoutingModule { }
