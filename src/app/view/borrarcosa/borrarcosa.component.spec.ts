import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BorrarcosaComponent } from './borrarcosa.component';

describe('BorrarcosaComponent', () => {
  let component: BorrarcosaComponent;
  let fixture: ComponentFixture<BorrarcosaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BorrarcosaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BorrarcosaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
