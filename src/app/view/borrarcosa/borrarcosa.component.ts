import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Item} from "../../shared/classes/Item";

@Component({
  selector: 'app-borrarcosa',
  templateUrl: './borrarcosa.component.html',
  styleUrls: ['./borrarcosa.component.css']
})
export class BorrarcosaComponent implements OnInit {
  @Input() item!:Item;
  itemImageUrl = 'https://static.educalingo.com/img/en/800/teapot.jpg';
  lineThrough = '';
  displayNone = '';
  constructor() { }
  @Input() prefix = '';
  @Output() deleteRequest = new EventEmitter<Item>();

  delete (){
    this.deleteRequest.emit(this.item);
    this.displayNone = this.displayNone ? '' : 'none';
    this.lineThrough = this.lineThrough ? '' : 'line-through'

  }

  ngOnInit(): void {
  }

}
