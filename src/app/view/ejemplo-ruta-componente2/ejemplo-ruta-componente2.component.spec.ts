import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EjemploRutaComponente2Component } from './ejemplo-ruta-componente2.component';

describe('EjemploRutaComponente2Component', () => {
  let component: EjemploRutaComponente2Component;
  let fixture: ComponentFixture<EjemploRutaComponente2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EjemploRutaComponente2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EjemploRutaComponente2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
