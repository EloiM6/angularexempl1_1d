import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {LoggerService} from "../../shared/services/logger.service";

@Component({
  selector: 'app-cambiotamano',
  templateUrl: './cambiotamano.component.html',
  styleUrls: ['./cambiotamano.component.css']
})
export class CambiotamanoComponent implements OnInit {
  @Input() size!: number | string;
  @Output() sizeChange = new EventEmitter<number>()
  constructor(private logger : LoggerService) { }

  ngOnInit(): void {
  }

  dec() {
    this.logger.log("Se li ha donat a decrementar");
    this.canvitamano(-1);
  }
  inc() {
    this.logger.log("Se li ha donat a incrementar");
    this.canvitamano(+1);
  }
  canvitamano(number: number) {
    /*
    Exemple de size
    Tinc un tamany de 15. Com aplica la formula?
    1. Math.max(8, +this.size + number) => maxim(8,15) =>15
    2. Math.min(40, Math.max(8, +this.size + number)); => minim (40,15) => 15

    Tinc un tamany de 1000. Com aplica la formula?
    1. Math.max(8, +this.size + number) => maxim(8,1000) =>1000
    2. Math.min(40, Math.max(8, +this.size + number)); => minim (40,1000) => 40

    Tinc un tamany de 5. Com aplica la formula?
    1. Math.max(8, +this.size + number) => maxim(8,5) => 8
    2. Math.min(40, Math.max(8, +this.size + number)); => minim (40,8) => 8

    */
    this.size = Math.min(40, Math.max(8, +this.size + number));
    this.sizeChange.emit(this.size);

  }
}
