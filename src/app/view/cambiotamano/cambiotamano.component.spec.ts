import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CambiotamanoComponent } from './cambiotamano.component';

describe('CambiotamanoComponent', () => {
  let component: CambiotamanoComponent;
  let fixture: ComponentFixture<CambiotamanoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CambiotamanoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CambiotamanoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    component.size = 35;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('mostrar el tamany', () => {
    let selectedNum: number | undefined;


    expect(component.size).toBe(35);
    component.dec();
    expect(component.size).toBe(34);

    component.canvitamano(125);
    expect(component.size).toBe(40);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('span').textContent).toBe("Tamany font: 40px");
  });
});
