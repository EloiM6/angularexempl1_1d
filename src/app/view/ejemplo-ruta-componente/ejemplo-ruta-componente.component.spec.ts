import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EjemploRutaComponenteComponent } from './ejemplo-ruta-componente.component';

describe('EjemploRutaComponenteComponent', () => {
  let component: EjemploRutaComponenteComponent;
  let fixture: ComponentFixture<EjemploRutaComponenteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EjemploRutaComponenteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EjemploRutaComponenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
