import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FillinjectatComponent } from './fillinjectat.component';

describe('FillinjectatComponent', () => {
  let component: FillinjectatComponent;
  let fixture: ComponentFixture<FillinjectatComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FillinjectatComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FillinjectatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
