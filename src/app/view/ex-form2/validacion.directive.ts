import {Directive, Input} from '@angular/core';
import {AbstractControl, NG_VALIDATORS, ValidationErrors, Validator, ValidatorFn} from "@angular/forms";

@Directive({
  selector: '[appValidacion]',
  providers: [{provide: NG_VALIDATORS, useExisting: ValidacionDirective, multi: true}]
})
export class ValidacionDirective implements Validator{
  @Input ('appValidacion') nombrecutre = false;
  constructor() { }
  validate(control: AbstractControl ){
    // Ejemplo Pol Como hago para ejecutar la parte de la derecha
    // que es cuando nombrecutre es falso
    // Para que eso hay que cambiar el orden de las proe
    // this.nombrecutre ? null:forbiddenNameValidator() (control)
    // o tmb !this.nombrecutre ? forbiddenNameValidator() (control):null;
    return this.nombrecutre ? forbiddenNameValidator() (control):null;
  }
}
export function forbiddenNameValidator(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    const value = control.value;
    if (!value) {
      return null;
    }
    const hasUpperCase = /[A-Z]+/.test(value);
    const hasLowerCase = /[a-z]+/.test(value);
    const hasNumeric = /[0-9]+/.test(value);
    const passwordValid = hasUpperCase && hasLowerCase && hasNumeric;
    return !passwordValid ? {passwordStrength:true}: null;
  };
}
