import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ConnectBDService} from "../../shared/services/connect-bd.service";
import {forbiddenNameValidator} from "./validacion.directive";

@Component({
  selector: 'app-ex-form2',
  templateUrl: './ex-form2.component.html',
  styleUrls: ['./ex-form2.component.css']
})
export class ExForm2Component implements OnInit {
  formgrup !: FormGroup;
  insertForm !: FormGroup;
  constructor(private connectbd:ConnectBDService) { }

  ngOnInit(): void {
    this.formgrup = new FormGroup({
      nombre : new FormControl('marc@ies-sabadell.cat', [Validators.required,
        Validators.minLength(5), Validators.email]),
      password : new FormControl('gat', [Validators.required,
        Validators.minLength(3), forbiddenNameValidator()])
    });

    this.insertForm = new FormGroup({
      nom : new FormControl('Buscaminas',
        [Validators.required, Validators.minLength(3)]),
      pegy : new FormControl('7',
        [Validators.required]),
      preu : new FormControl('1000',
        [Validators.required])
    })
  }

  get nombre() {
    return this.formgrup.get('nombre');
  }
  get password() {
    return this.formgrup.get('password');
  }
  get nom() {
    return this.insertForm.get('nom');
  }
  get pegy() {
    return this.insertForm.get('pegy');
  }
  get preu() {
    return this.insertForm.get('preu');
  }


  dale() {
    let name:string = this.nombre?.value;
    console.log(name)
    /*this.connectbd.conectarBDGet(name).subscribe( res => {
        console.log(res['data'][0]['alias']);
      })*/
    this.connectbd.conectarBDPost(this.formgrup.value).subscribe( res => {
      console.log(res);
      console.log(res['data'][0]['alias']);
    })

  }

  Insert() {

    this.connectbd.conectarBDInsert(this.insertForm.value).subscribe( res => {
      console.log(res);

    })

  }
}
