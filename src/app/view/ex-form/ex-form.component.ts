import { Component, OnInit } from '@angular/core';
import {FormControl} from "@angular/forms";

@Component({
  selector: 'app-ex-form',
  templateUrl: './ex-form.component.html',
  styleUrls: ['./ex-form.component.css']
})
export class ExFormComponent implements OnInit {

  constructor() { }
  name = new FormControl('Marc');
  updateName() {
    this.name.setValue('Profe');
  }
  ngOnInit(): void {
  }


}
