import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Ejemplo1Component } from './view/ejemplo1/ejemplo1.component';
import { FillinjectatComponent } from './view/fillinjectat/fillinjectat.component';
import { CambiotamanoComponent } from './view/cambiotamano/cambiotamano.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { BorrarcosaComponent } from './view/borrarcosa/borrarcosa.component';
import { AppRoutingModule } from './app-routing.module';
import { EjemploRutaComponenteComponent } from './view/ejemplo-ruta-componente/ejemplo-ruta-componente.component';
import { EjemploRutaComponente2Component } from './view/ejemplo-ruta-componente2/ejemplo-ruta-componente2.component';
import {HttpClientModule} from "@angular/common/http";
import { ExFormComponent } from './view/ex-form/ex-form.component';
import {ExForm2Component} from "./view/ex-form2/ex-form2.component";
import { ValidacionDirective } from './view/ex-form2/validacion.directive';



@NgModule({
  declarations: [
    AppComponent,
    Ejemplo1Component,
    FillinjectatComponent,
    CambiotamanoComponent,
    BorrarcosaComponent,
    EjemploRutaComponenteComponent,
    EjemploRutaComponente2Component,
    ExFormComponent,
    ExForm2Component,
    ValidacionDirective

  ],
  imports: [
    BrowserModule,
    NgbModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
