import { Component } from '@angular/core';
import { Item } from './shared/classes/Item';
import { Hero,  heroesdeldesierto } from './shared/interfaces/Hero';
import {LoggerService} from "./shared/services/logger.service";
import {ConnectBDService} from "./shared/services/connect-bd.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private logger : LoggerService, private conBD:ConnectBDService) {
  }
  title = 'Exemple1';
  itemImageUrl = 'https://www.biografias.es/files/star/thumb/v/vladimir-puttin_vfkdd.jpg'
  ItemPadre = 'Esto es un elemento enlazado que va al hijo';
  valorp!:string;
  valorp2:string = "";

  fontSizePx = 16;

  heroes = heroesdeldesierto;
  // Per inicialitzar un primer valor desde hero
  // Aquesta variable és per mostrar el funcionament de ngif
  hero: Hero | null = this.heroes[1];

  // Per comprovar què passi si no té valor
  //hero!: Hero | null;

  onclick($event?: MouseEvent, numboton?:number) {
    if (numboton==1){
      this.valorp="Esto es una idea loca del profe";
      this.logger.log("Li he donat al primer botó");
    } else if (numboton==2){
      this.valorp2="<i>Esto no es un texto</i>"
      this.logger.log("Li he donat al segon botó");
    }


    alert("Esdeveniment capturat");
  }

  currentItem = { name: 'Tetera'} ;


  deleteItem(item: Item) {
    alert(`Delete the ${item.name}.`);
  }

  callPhone(value: string) {
    console.log("El valor de la variable phone: " + value)
  }

  connectBD1() {
    this.conBD.conectarBD().subscribe(respuesta => {this.logger.log(respuesta)});
  }
}
